import { Message } from './message';

export interface IMessageAggregate {
  recentMessages?: Message[];
  lastMessageActivity?: Date;
  isLastMessageIncoming?: boolean;
  isIncomingStreak?: number;
  unreadTimestamp?: Date;
}

export class MessageAggregate implements IMessageAggregate {
  recentMessages: Message[];
  lastMessageActivity: Date;
  isLastMessageIncoming: boolean;
  isIncomingStreak: number;
  unreadTimestamp: Date;

  constructor(
    recentMessages = null,
    lastMessageActivity = null,
    isLastMessageIncoming = null,
    isIncomingStreak = null,
    unreadTimestamp = null
  ) {
    if (recentMessages != null) {
      this.recentMessages = recentMessages;
    }
    if (lastMessageActivity != null) {
      this.lastMessageActivity = lastMessageActivity;
    }
    if (isLastMessageIncoming != null) {
      this.isLastMessageIncoming = isLastMessageIncoming;
    }
    if (isIncomingStreak != null) {
      this.isIncomingStreak = isIncomingStreak;
    }
    if (unreadTimestamp != null) {
      this.unreadTimestamp = unreadTimestamp;
    }
  }

  public static From(m: any): MessageAggregate {
    const { recentMessages, lastMessageActivity, isLastMessageIncoming, isIncomingStreak, unreadTimestamp } = m;
    return new this(recentMessages, lastMessageActivity, isLastMessageIncoming, isIncomingStreak, unreadTimestamp);
  }
}
