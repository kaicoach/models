export enum ChangeActionType {
    ADDED = 'added',
    MODIFIED = 'modified',
    REMOVED = 'removed'
  }
  export class ChangeAction<T> {
    constructor(public type: ChangeActionType, public data: T) {}
  }
  