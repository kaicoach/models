import { MessageAggregate } from './messageAggregate';

export interface IHero {
  customerId?: string;
  birthdate?: Date;
  expiration?: Date;
  cellPhone?: string;
  email?: string;
  outgoingNumber?: string;
  firstName?: string;
  lastName?: string;
  gender?: boolean;
  goal?: string;
  id?: string;
  imageUrl?: string;
  snooze?: boolean;
  messageAggregate?: MessageAggregate;
}

export class Hero implements IHero {
  customerId: string;
  birthdate: Date;
  expiration: Date;
  cellPhone: string;
  email: string;
  outgoingNumber: string;
  firstName: string;
  lastName: string;
  gender: boolean;
  goal: string;
  id: string;
  imageUrl: string;
  snooze: boolean;
  messageAggregate: MessageAggregate;

  constructor(
    customerId = null,
    birthdate = null,
    expiration = null,
    cellPhone = null,
    email = null,
    outgoingNumber = null,
    firstName = null,
    lastName = null,
    gender = null,
    goal = null,
    id = null,
    imageUrl = null,
    snooze = null,
    messageAggregate = null
  ) {
    if (customerId != null) {
      this.customerId = customerId;
    }
    if (birthdate != null) {
      this.birthdate = birthdate;
    }
    if (expiration != null) {
      this.expiration = expiration;
    }
    if (cellPhone != null) {
      this.cellPhone = cellPhone;
    }
    if (email != null) {
      this.email = email;
    }
    if (outgoingNumber != null) {
      this.outgoingNumber = outgoingNumber;
    }
    if (firstName != null) {
      this.firstName = firstName;
    }
    if (lastName != null) {
      this.lastName = lastName;
    }
    if (gender != null) {
      this.gender = gender;
    }
    if (goal != null) {
      this.goal = goal;
    }
    if (id != null) {
      this.id = id;
    }
    if (imageUrl != null) {
      this.imageUrl = imageUrl;
    }
    if (snooze != null) {
      this.snooze = snooze;
    }
    if (messageAggregate != null) {
      this.messageAggregate = messageAggregate;
    }
  }

  public static From(h: any): Hero {
    const {
      customerId,
      birthdate,
      expiration,
      cellPhone,
      email,
      outgoingNumber,
      firstName,
      lastName,
      gender,
      goal,
      id,
      imageUrl,
      snooze,
      messageAggregate
    } = h;
    return new this(
      customerId,
      birthdate,
      expiration,
      cellPhone,
      email,
      outgoingNumber,
      firstName,
      lastName,
      gender,
      goal,
      id,
      imageUrl,
      snooze,
      messageAggregate
    );
  }

  getAge = () => {
    if (this.birthdate) {
      const timeDiff = Math.abs(Date.now() - this.birthdate.getSeconds());
      return Math.floor(timeDiff / (1000 * 3600 * 24) / 365);
    } else {
      return null;
    }
  };
}
