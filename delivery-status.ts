export enum DeliveryStatus {
  CREATED = 100,
  SENT = 200,
  DELIVERED = 300,
  ERROR = 400
}
