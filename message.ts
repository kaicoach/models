import { DeliveryStatus } from './delivery-status';

export interface IMessage {
  agent?: string;
  cellPhone?: string;
  createdAt?: Date;
  datetime?: Date;
  isIncoming?: boolean;
  isSystem?: boolean;
  updatedAt?: Date;
  body?: string;
  id?: string;
  outgoingNumber?: string;
  media?: any;
  sid?: string;
  status?: DeliveryStatus;
  sent?: boolean;
  delivered?: boolean;
  error?: boolean;
}

export class Message implements IMessage {
  agent: string;
  cellPhone: string;
  createdAt: Date;
  datetime: Date;
  isIncoming: boolean;
  updatedAt: Date;
  isSystem: boolean;
  body: string;
  id: string;
  outgoingNumber: string;
  media: any;
  sid: string;
  status: DeliveryStatus;
  sent?: boolean;
  delivered?: boolean;
  error?: boolean;

  constructor(
    agent: string = null,
    cellPhone: string = null,
    createdAt: Date = null,
    datetime: Date = null,
    isIncoming: boolean = null,
    updatedAt: Date = null,
    isSystem: boolean = null,
    body: string = null,
    id: string = null,
    outgoingNumber: string = null,
    media: any = null,
    sid: string = null,
    sent: boolean = false,
    delivered: boolean = false,
    error: boolean = false
  ) {
    if (this.agent != null) {
      this.agent = agent;
    }
    if (cellPhone != null) {
      this.cellPhone = cellPhone;
    }
    if (createdAt != null) {
      this.createdAt = createdAt;
    }
    if (datetime != null) {
      this.datetime = datetime;
    }
    if (isIncoming != null) {
      this.isIncoming = isIncoming;
    }
    if (updatedAt != null) {
      this.updatedAt = updatedAt;
    }
    if (isSystem != null) {
      this.isSystem = isSystem;
    }
    if (body != null) {
      this.body = body;
    }
    if (id != null) {
      this.id = id;
    }
    if (outgoingNumber != null) {
      this.outgoingNumber = outgoingNumber;
    }
    if (media != null) {
      this.media = media;
    }
    if (sid != null) {
      this.sid = sid;
    }
    if (sent != null) {
      this.sent = sent;
    }
    if (delivered != null) {
      this.delivered = delivered;
    }
    if (error != null) {
      this.error = error;
    }

    if (error) {
      this.status = DeliveryStatus.ERROR;
    } else if (delivered) {
      this.status = DeliveryStatus.DELIVERED;
    } else if (sent) {
      this.status = DeliveryStatus.SENT;
    }
  }

  public static From(m: any): Message {
    const {
      agent,
      cellPhone,
      createdAt,
      datetime,
      isIncoming,
      updatedAt,
      isSystem,
      body,
      id,
      outgoingNumber,
      media,
      sid,
      sent,
      delivered,
      error
    } = m;
    return new this(
      agent,
      cellPhone,
      createdAt,
      datetime,
      isIncoming,
      updatedAt,
      isSystem,
      body,
      id,
      outgoingNumber,
      media,
      sid,
      sent,
      delivered,
      error
    );
  }
}
